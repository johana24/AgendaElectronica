/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procedimientoAgenda;
 
import Principal.*;
/**
 *
 * @author Wendy
 */
public class Persona  {
    private String nombre;
    private String apellido;
    private int telefono;
    private String direccion;
    private String genero;
    
  public Persona()
    {
    this.nombre=null;
    this.apellido=null;
    this.direccion=null;
    this.genero=null;
    this.telefono=0;
    
    }
    public Persona(String nombre, String apellido, int telefono) {
        this.nombre = nombre;
        this.apellido=apellido;
        this.telefono = telefono;
        this.direccion= direccion;
        this.genero=genero;
    }
    public void set_nombre(String nomb){        
        this.nombre=nomb.toUpperCase();
    }
    public void set_apellido(String apellido){
        this.apellido= apellido;
    } 
    public void set_telefono(int telf){
        this.telefono=telf;
    }
    
    public String getNombre() {
        return this.nombre;
    }
    public String getApellido() {
        return this.apellido;
    }
 
    public int getTelefono() {
        return telefono;
    }    
    
     public void set_direccion(String direccion){        
        this.direccion=direccion;
    }
    public void set_genero(String genero){
        this.genero= genero;
    } 
    public String getDireccion() {
        return this.direccion;
    }
    public String getGenero() {
        return this.genero;
    }
 
  
}